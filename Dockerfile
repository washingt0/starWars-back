FROM golang:1.17-alpine

WORKDIR /back

ADD go.mod .
ADD go.sum .

RUN go mod download

ADD . .

RUN go build -o starwars

EXPOSE 8080
ENTRYPOINT /back/starwars