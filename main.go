package main

import (
	"log"
	"starwars/database"
	"starwars/handler"

	"github.com/gin-gonic/gin"
)

func main() {
	var err error

	if err = database.InitDB(); err != nil {
		log.Fatal("não foi possível inicializar o banco", err)
	}
	defer database.CloseDB()

	router := gin.Default()

	handler.HTTPRouter(router.Group("v1"))

	router.Run()
}
