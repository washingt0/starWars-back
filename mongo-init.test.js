db.planetsAppearances.insertMany(
    [
        {"name":"Tatooine","films":5}, {"name":"Alderaan","films":2}, {"name":"Yavin IV","films":1},
        {"name":"Hoth","films":1}, {"name":"Dagobah","films":3}, {"name":"Bespin","films":1},
        {"name":"Endor","films":1}, {"name":"Naboo","films":4}, {"name":"Coruscant","films":4},
        {"name":"Kamino","films":1}, {"name":"Geonosis","films":1}, {"name":"Utapau","films":1},
        {"name":"Mustafar","films":1}, {"name":"Kashyyyk","films":1}, {"name":"Polis Massa","films":1},
        {"name":"Mygeeto","films":1}, {"name":"Felucia","films":1}, {"name":"Cato Neimoidia","films":1},
        {"name":"Saleucami","films":1}, {"name":"Stewjon","films":0}, {"name":"Eriadu","films":0},
        {"name":"Corellia","films":0}, {"name":"Rodia","films":0}, {"name":"Nal Hutta","films":0},
        {"name":"Dantooine","films":0}, {"name":"Bestine IV","films":0}, {"name":"Ord Mantell","films":1},
        {"name":"unknown","films":0}, {"name":"Trandosha","films":0}, {"name":"Socorro","films":0},
        {"name":"Mon Cala","films":0}, {"name":"Chandrila","films":0}, {"name":"Sullust","films":0},
        {"name":"Toydaria","films":0}, {"name":"Malastare","films":0}, {"name":"Dathomir","films":0},
        {"name":"Ryloth","films":0}, {"name":"Aleen Minor","films":0}, {"name":"Vulpter","films":0},
        {"name":"Troiken","films":0}, {"name":"Tund","films":0}, {"name":"Haruun Kal","films":0},
        {"name":"Cerea","films":0}, {"name":"Glee Anselm","films":0}, {"name":"Iridonia","films":0},
        {"name":"Tholoth","films":0}, {"name":"Iktotch","films":0}, {"name":"Quermia","films":0},
        {"name":"Dorin","films":0}, {"name":"Champala","films":0}, {"name":"Mirial","films":0},
        {"name":"Serenno","films":0}, {"name":"Concord Dawn","films":0}, {"name":"Zolan","films":0},
        {"name":"Ojom","films":0}, {"name":"Skako","films":0}, {"name":"Muunilinst","films":0},
        {"name":"Shili","films":0}, {"name":"Kalee","films":0}, {"name":"Umbara","films":0}
    ]
);

db.planets.insertMany(
    [
        {"name":"Tatooine","terrain": "TEST", "climate": "TEST"}, {"name":"Alderaan","terrain": "TEST", "climate": "TEST"},
        {"name":"Skako","terrain": "TEST", "climate": "TEST"}, {"name":"teste","terrain": "TEST", "climate": "TEST"}
    ]
);