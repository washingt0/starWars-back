#! /bin/bash

function fetch() {
    echo "Page $1"
    curl -X GET "https://swapi.dev/api/planets?page=$1" | jq -c '.results[] | {name, films: (.films|length)}' >> planets_appearences
}

page=1

fetch $page

while [ $? -eq 0 ]; do
    let "page++"
    fetch $page
done

if [[ $(wc -l <planets_appearences) -ne 60 ]]; then
    echo "Unable to fetch planets"
    exit 1
fi

echo "Planets fetched"

jq -c '. |= [inputs]' planets_appearences> planets_appearences.json
