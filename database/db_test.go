package database

import (
	"context"
	"testing"

	"go.mongodb.org/mongo-driver/mongo"

	"go.mongodb.org/mongo-driver/mongo/readpref"
)

func TestCloseDB(t *testing.T) {
	var err error

	if err = InitDB(); err != nil {
		t.Fail()
	}

	CloseDB()

	conn := GetConn()

	if err = conn.Ping(context.Background(), readpref.Primary()); err == nil {
		t.FailNow()
	}
}

func TestGetConn(t *testing.T) {
	var err error

	if err = InitDB(); err != nil {
		t.Fail()
	}

	var c *mongo.Client

	c = GetConn()

	if c == nil {
		t.FailNow()
	}

	CloseDB()
}

func TestInitDB(t *testing.T) {
	var err error

	if err = InitDB(); err != nil {
		t.FailNow()
	}

	if err = GetConn().Ping(context.Background(), readpref.Primary()); err != nil {
		t.FailNow()
	}
}
