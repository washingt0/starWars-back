package database

import (
	"context"
	"log"
	"os"

	"go.mongodb.org/mongo-driver/mongo/readpref"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var clt *mongo.Client // nolint

// InitDB iniciliza o pool de conexões com o banco de dados
func InitDB() (err error) {
	var connStr = "mongodb://localhost:27017/starwars"

	if env := os.Getenv("STARWARS_MONGO_URI"); env != "" {
		connStr = env
	}

	if clt, err = mongo.Connect(context.Background(), options.Client().ApplyURI(connStr)); err != nil {
		return
	}

	if err = clt.Ping(context.Background(), readpref.Primary()); err != nil {
		return
	}

	return
}

// CloseDB fecha as conexões com o banco
func CloseDB() {
	if clt == nil {
		return
	}

	_ = clt.Disconnect(context.Background())
}

// GetConn retorna a conexão com o banco
func GetConn() *mongo.Client {
	if clt == nil {
		log.Fatal("banco não inicializado")
	}

	return clt
}
