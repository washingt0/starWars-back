package utils

import (
	"log"
	"net/http"

	"github.com/go-playground/validator/v10"

	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/mongo"
)

// ErrorHandling realiza o tratamento/log dos erros
func ErrorHandling(c *gin.Context, msg string, err error) {
	var (
		statusCode = http.StatusInternalServerError
	)

	if err == mongo.ErrNoDocuments {
		statusCode = http.StatusNotFound
	}

	if _, ok := err.(validator.ValidationErrors); ok {
		statusCode = http.StatusBadRequest
	}

	log.Println("[ERROR]: ", err)

	c.AbortWithStatusJSON(statusCode, gin.H{
		"msg": msg,
	})
}
