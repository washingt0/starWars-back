package utils

import (
	"errors"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/go-playground/validator/v10"

	"go.mongodb.org/mongo-driver/mongo"

	"github.com/gin-gonic/gin"
)

func TestErrorHandling(t *testing.T) {
	type args struct {
		msg string
		err error
	}
	tests := []struct {
		name     string
		args     args
		expected int
	}{
		{
			name: "NODOCUMENT",
			args: args{
				err: mongo.ErrNoDocuments,
				msg: "TEST_MESSAGE",
			},
			expected: http.StatusNotFound,
		},
		{
			name: "GENERAL",
			args: args{
				err: errors.New("TEST_ERROR"),
				msg: "TEST_MESSAGE",
			},
			expected: http.StatusInternalServerError,
		},
		{
			name: "FORM-ERROR",
			args: args{
				err: validator.ValidationErrors{},
				msg: "TEST_MESSAGE",
			},
			expected: http.StatusBadRequest,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c, _ := gin.CreateTestContext(httptest.NewRecorder())

			ErrorHandling(c, tt.args.msg, tt.args.err)

			if c.Writer.Status() != tt.expected {
				t.FailNow()
			}
		})
	}
}
