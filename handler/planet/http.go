package planet

import (
	"net/http"
	"starwars/application/planet"
	"starwars/utils"

	"github.com/gin-gonic/gin"
)

// Router roteador da subdivisao de planetas
func Router(r *gin.RouterGroup) {
	r.POST("", add)
	r.GET("", list)
	r.GET(":id", get)
	r.DELETE(":id", remove)
}

// Listar planetas
// GET /v1/planets
// ?name=XXXX busca por nome do planeta
func list(c *gin.Context) {
	var (
		data   *planet.List
		search *string
		err    error
	)

	if vv := c.Query("name"); vv != "" {
		search = &vv
	}

	if data, err = planet.GetAll(c.Copy(), search); err != nil {
		utils.ErrorHandling(c, "Não foi possível buscar os planetas", err)
		return
	}

	c.JSON(http.StatusOK, data)
}

// adiciona planeta
// POST /v1/planets
// Content-Type: application/json
// {
// 	"name": "Alderran",
// 	"terrain": "forests",
// 	"climate": "tropical"
// }
func add(c *gin.Context) {
	var (
		in  *planet.Planet
		out *planet.OnlyID
		err error
	)

	if err = c.ShouldBindJSON(&in); err != nil {
		utils.ErrorHandling(c, "Não foi possível obter os dados da requisição", err)
		return
	}

	if out, err = planet.Add(c.Copy(), in); err != nil {
		utils.ErrorHandling(c, "Não foi possível adicionar o planeta, verifique se o nome bate com algum planeta conhecido", err)
		return
	}

	c.JSON(http.StatusCreated, out)
}

// obtem um planeta pelo ID
// GET /v1/planets/{id}
func get(c *gin.Context) {
	var (
		data *planet.Planet
		id   string
		err  error
	)

	id = c.Param("id")

	if data, err = planet.Get(c.Copy(), id); err != nil {
		utils.ErrorHandling(c, "Não foi possível buscar o planeta", err)
		return
	}

	c.JSON(http.StatusOK, data)
}

// remove um planeta pelo ID
// DELETE /v1/planets/{id}
func remove(c *gin.Context) {
	var (
		id  string
		err error
	)

	id = c.Param("id")

	if err = planet.Delete(c.Copy(), id); err != nil {
		utils.ErrorHandling(c, "Não foi possível remover o planeta", err)
		return
	}

	c.JSON(http.StatusNoContent, nil)
}
