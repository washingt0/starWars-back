package planet

import (
	"bytes"
	"context"
	"encoding/json"
	"log"
	"net/http"
	"net/http/httptest"
	"starwars/database"
	"testing"

	"go.mongodb.org/mongo-driver/bson/primitive"

	"github.com/go-playground/assert/v2"

	"github.com/gin-gonic/gin"
)

func init() {
	if err := database.InitDB(); err != nil {
		log.Fatal(err)
	}
}

func Test_add(t *testing.T) {
	r := gin.Default()
	Router(r.Group("v1/planets"))

	var data []byte

	data, _ = json.Marshal(map[string]interface{}{
		"name":    "alderaan",
		"terrain": "TEST",
		"climate": "TEST",
	})

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("POST", "/v1/planets", bytes.NewBuffer(data))

	r.ServeHTTP(w, req)

	assert.Equal(t, w.Code, http.StatusCreated)
}

func Test_delete(t *testing.T) {
	id := aux()

	r := gin.Default()
	Router(r.Group("v1/planets/"))

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("DELETE", "/v1/planets/"+id, bytes.NewBuffer([]byte{}))

	r.ServeHTTP(w, req)

	assert.Equal(t, w.Code, http.StatusNoContent)
}

func Test_get(t *testing.T) {
	id := aux()

	r := gin.Default()
	Router(r.Group("v1/planets/"))

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/v1/planets/"+id, bytes.NewBuffer([]byte{}))

	r.ServeHTTP(w, req)

	assert.Equal(t, w.Code, http.StatusOK)
}

func Test_list(t *testing.T) {
	r := gin.Default()
	Router(r.Group("v1/planets"))

	t.Run("GetAll", func(t *testing.T) {
		w := httptest.NewRecorder()
		req, _ := http.NewRequest("GET", "/v1/planets", bytes.NewBuffer([]byte{}))

		r.ServeHTTP(w, req)

		assert.Equal(t, w.Code, http.StatusOK)
	})

	t.Run("GetAllSearch", func(t *testing.T) {
		w := httptest.NewRecorder()
		req, _ := http.NewRequest("GET", "/v1/planets?name=alderaan", bytes.NewBuffer([]byte{}))

		r.ServeHTTP(w, req)

		assert.Equal(t, w.Code, http.StatusOK)
	})

}

func aux() string {
	var x struct {
		ID primitive.ObjectID `bson:"_id"`
	}

	if err := database.GetConn().Database("starwars").Collection("planets").FindOne(context.Background(), primitive.M{}).Decode(&x); err != nil {
		log.Fatal(err)
	}

	return x.ID.Hex()
}
