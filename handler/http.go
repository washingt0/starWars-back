package handler

import (
	"starwars/handler/planet"

	"github.com/gin-gonic/gin"
)

// HTTPRouter principal roteador http (raiz após a versão da API)
func HTTPRouter(r *gin.RouterGroup) {
	planet.Router(r.Group("planets"))
}
