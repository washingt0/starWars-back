## STAR WARS

### Testes:

```shell
$ docker-compose -f docker-compose.test.yml up --build
```


### Aplicação

```shell
$ docker-compose up --build
```

Porta padrão: 8080

### Endpoints:

#### Criar Planeta:
```http request
POST /v1/planets
Content-Type: application/json
{
    "name": "Nome do Planetas (deve ser um dos 60 fornecidos pela swapi.dev",
    "terrain": "Terreno do planeta",
    "climate": "Clima do planeta"
}

Status-Code: 201 Created
Content-Type: application/json
{
    "id": "id do planeta"
}
```
¹ Todos os campos são obrigatórios.

² `name` deve ter entre 3 e 50 caracteres

#### Listar Planetas:
```http request
GET /v1/planets
Status-Code: 200 Created
Content-Type: application/json
{
    "data": [
        {
            "id": "id do planeta"
            "name": "Nome do Planetas (deve ser um dos 60 fornecidos pela swapi.dev",
            "terrain": "Terreno do planeta",
            "climate": "Clima do planeta",
            "films": "quantidade de filmes em que o planeta apareceu (inteiro)"
        }
    ]
}
```
¹ Query-param opcional `name` onde filtrará os resultador por nome do planeta.

#### Buscar Planeta por ID:
```http request
GET /v1/planets/{id}
Status-Code: 200 Created
Content-Type: application/json
{
    "id": "id do planeta"
    "name": "Nome do Planetas (deve ser um dos 60 fornecidos pela swapi.dev",
    "terrain": "Terreno do planeta",
    "climate": "Clima do planeta",
    "films": "quantidade de filmes em que o planeta apareceu (inteiro)"
}
```

#### Remover Planeta:
```http request
DELETE /v1/planets/{id}
Status-Code: 204 Created
```
