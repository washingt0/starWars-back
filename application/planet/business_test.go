package planet

import (
	"context"
	"starwars/database"
	"testing"
)

func TestGetAll(t *testing.T) {
	var (
		err error
		out *List
	)

	if err = database.InitDB(); err != nil {
		t.Fail()
	}
	defer database.CloseDB()

	t.Run("TestGetAll", func(t *testing.T) {
		if out, err = GetAll(context.Background(), nil); err != nil {
			t.FailNow()
		}

		if len(out.Data) == 0 {
			t.FailNow()
		}
	})

	t.Run("TestGelAllSearch", func(t *testing.T) {
		var search = "test"
		if out, err = GetAll(context.Background(), &search); err != nil {
			t.FailNow()
		}

		if len(out.Data) == 0 {
			t.FailNow()
		}
	})
}

func TestDelete(t *testing.T) {
	var (
		err error
		id  *OnlyID
	)

	if err = database.InitDB(); err != nil {
		t.Fail()
	}
	defer database.CloseDB()

	if id, err = Add(context.Background(), &Planet{Name: "Alderaan", Terrain: "TESTE_DELETE", Climate: "TESTE_DELETE"}); err != nil {
		t.Error(err)
		t.FailNow()
	}

	if err = Delete(context.Background(), id.ID.Hex()); err != nil {
		t.Error(err)
		t.FailNow()
	}

	if _, err = Get(context.Background(), id.ID.Hex()); err == nil {
		t.FailNow()
	}
}

func TestGet(t *testing.T) {
	var (
		err error
		id  *OnlyID
	)

	if err = database.InitDB(); err != nil {
		t.Fail()
	}
	defer database.CloseDB()

	if id, err = Add(context.Background(), &Planet{Name: "Alderaan", Terrain: "TESTE_DELETE", Climate: "TESTE_DELETE"}); err != nil {
		t.Error(err)
		t.FailNow()
	}

	if _, err = Get(context.Background(), id.ID.Hex()); err != nil {
		t.Error(err)
		t.FailNow()
	}
}

func TestAdd(t *testing.T) {
	var (
		err error
	)

	if err = database.InitDB(); err != nil {
		t.Fail()
	}
	defer database.CloseDB()

	t.Run("OK", func(t *testing.T) {
		var id *OnlyID

		if id, err = Add(context.Background(), &Planet{Name: "Alderaan", Terrain: "TESTE_DELETE", Climate: "TESTE_DELETE"}); err != nil {
			t.Error(err)
			t.FailNow()
		}

		if _, err = Get(context.Background(), id.ID.Hex()); err != nil {
			t.Error(err)
			t.FailNow()
		}
	})

	t.Run("PLANET NOT FOUND", func(t *testing.T) {
		if _, err = Add(context.Background(), &Planet{Name: "PLANET NOT FOUND", Terrain: "TESTE_DELETE", Climate: "TESTE_DELETE"}); err == nil {
			t.Error(err)
			t.FailNow()
		}
	})
}
