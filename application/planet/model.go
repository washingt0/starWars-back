package planet

import "go.mongodb.org/mongo-driver/bson/primitive"

// Planet estrutura básica de um planeta
type Planet struct {
	ID      primitive.ObjectID `bson:"_id,omitempty" json:"id"`
	Name    string             `bson:"name" json:"name" binding:"required,gte=3,lte=50"`
	Climate string             `bson:"climate" json:"climate" binding:"required"`
	Terrain string             `bson:"terrain" json:"terrain" binding:"required"`
	Films   int                `bson:"films,omitempty" json:"films"`
}

// List listagem de planetas
type List struct {
	Data []Planet `json:"data"`
}

// OnlyID retorna apenas o ID do objeto criado
type OnlyID struct {
	ID primitive.ObjectID `bson:"_id" json:"id"`
}

// appearance estrutura interna para obter a quantidade de filmes em que um planeta apareceu
type appearance struct {
	Name  string `bson:"name"`
	Films int    `bson:"films"`
}
