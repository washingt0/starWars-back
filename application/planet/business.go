package planet

import (
	"context"
	"starwars/database"

	"go.mongodb.org/mongo-driver/bson/primitive"

	"go.mongodb.org/mongo-driver/mongo"
)

// GetAll recupera do banco de dados todos os planetas
func GetAll(ctx context.Context, search *string) (out *List, err error) {
	var (
		db     = database.GetConn().Database("starwars")
		coll   = db.Collection("planets")
		cur    *mongo.Cursor
		filter = primitive.M{}
	)

	if search != nil {
		filter["name"] = primitive.M{
			"$regex": primitive.Regex{
				Pattern: ".*" + *search + ".*",
				Options: "i",
			},
		}
	}

	if cur, err = coll.Find(ctx, filter); err != nil {
		return
	}
	defer func() { _ = cur.Close(ctx) }()

	out = new(List)
	out.Data = make([]Planet, 0, cur.RemainingBatchLength())

	for cur.Next(ctx) {
		var p Planet
		if err = cur.Decode(&p); err != nil {
			return
		}

		out.Data = append(out.Data, p)
	}

	return
}

// Add persiste planeta no banco de dados
func Add(ctx context.Context, p *Planet) (out *OnlyID, err error) {
	var (
		db        = database.GetConn().Database("starwars")
		collFilms = db.Collection("planetsAppearances")
		coll      = db.Collection("planets")
		res       *mongo.InsertOneResult
		ap        appearance
	)

	if err = collFilms.FindOne(ctx, primitive.M{
		"name": primitive.M{
			"$regex": primitive.Regex{
				Pattern: p.Name,
				Options: "i",
			},
		},
	}).Decode(&ap); err != nil {
		return
	}

	p.Films = ap.Films

	if res, err = coll.InsertOne(ctx, p); err != nil {
		return
	}

	return &OnlyID{
		ID: res.InsertedID.(primitive.ObjectID),
	}, nil
}

// Get recupera um planeta pelo ID
func Get(ctx context.Context, id string) (out *Planet, err error) {
	var (
		db    = database.GetConn().Database("starwars")
		coll  = db.Collection("planets")
		objID primitive.ObjectID
	)

	if objID, err = primitive.ObjectIDFromHex(id); err != nil {
		return
	}

	if err = coll.FindOne(ctx, primitive.M{
		"_id": objID,
	}).Decode(&out); err != nil {
		return
	}

	return
}

// Delete remove um planeta pelo ID
func Delete(ctx context.Context, id string) (err error) {
	var (
		db    = database.GetConn().Database("starwars")
		coll  = db.Collection("planets")
		objID primitive.ObjectID
	)

	if objID, err = primitive.ObjectIDFromHex(id); err != nil {
		return
	}

	if _, err = coll.DeleteOne(ctx, primitive.M{
		"_id": objID,
	}); err != nil {
		return
	}

	return
}
